import React from 'react';
import './Nav.css';
import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav>
      <ul className="nav-links">
        <li style={{ marginRight: '1rem' }}>
          <a href="/home">Conference GO!</a>
        </li>
        <li style={{ marginRight: '1rem' }}>
          <NavLink to="/home">Home</ NavLink>
        </li>
        <li style={{ marginRight: '1rem' }}>
          <NavLink to="/locations/new">New location</NavLink>
        </li>
        <li style={{ marginRight: '1rem' }}>
          <NavLink to="/conferences/new">New conference</NavLink>
        </li>
        <li>
          <NavLink to="/presentations/new">New presentation</NavLink>
        </li>
      </ul>
    </nav>
  );
}

export default Nav;

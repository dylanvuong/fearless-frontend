import React, { useState } from 'react';

export default function LocationForm() {
  const [locationData, setLocationData] = useState({
    name: '',
    address: '',
    // add other form fields as needed
  });

  const handleChange = (e) => {
    setLocationData({
      ...locationData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Handle form submission logic here
    // You can access form data using the `locationData` state
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input
          type="text"
          name="name"
          value={locationData.name}
          onChange={handleChange}
        />
      </label>
      <label>
        Address:
        <input
          type="text"
          name="address"
          value={locationData.address}
          onChange={handleChange}
        />
      </label>
      {/* Add other form fields as needed */}
      <button type="submit">Submit</button>
    </form>
  );
}

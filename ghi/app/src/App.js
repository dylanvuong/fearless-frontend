import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <Router>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/home" element={<MainPage />} />
          <Route path="/presentations/new" element={<PresentationForm />} /> 
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;